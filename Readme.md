# Load Hashes to SQLite Database

Create file of SHA256 hashes of directory

    find myfiles -type f -exec sha256sum {} \; > ~/myfiles.checksums

Import to SQLite3 database

    python3 <name-of-sqlite-file> <host> <checksum-file>