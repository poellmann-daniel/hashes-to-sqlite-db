import sqlite3
import sys
from sqlite3 import Error


def run(dbfile, host, hashfile):
    conn = None
    try:
        conn = sqlite3.connect(dbfile)
    except Error as e:
        print(e)
        return None

    c = conn.cursor()
    c.execute("""
    CREATE TABLE IF NOT EXISTS "files" (
        "id"	INTEGER,
        "hash"	TEXT NOT NULL,
        "file"	TEXT NOT NULL,
        "host"	TEXT NOT NULL,
        PRIMARY KEY("id" AUTOINCREMENT)
    )""")

    with open(hashfile) as file:
        rows = []
        counter = 0
        while line := file.readline().rstrip():
            counter = counter + 1
            parts = line.split('  ', 1)
            print(parts[0] + "  " + parts[1])
            rows.append((parts[0], parts[1], host))

            if counter % 100 == 0:
                insertIntoDB(conn, rows)
                rows = []

        insertIntoDB(conn, rows)
        rows = []


def insertIntoDB(conn, rows):
    c = conn.cursor()
    c.executemany('INSERT INTO files (hash, file, host) values (?,?,?)', rows)
    conn.commit()


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2], sys.argv[3])

